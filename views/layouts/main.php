<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\menu;

AppAsset::register($this);
$this->registerCssFile("@web/css/mio.css",[
        'depends' => [\yii\bootstrap\BootstrapAsset::className()]]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-fixed-top navbar-default',
            'id' => 'custom-bootstrap-menu'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Administrar', 'url' => ['/site/administrar']],
            
            ['label' => 'Listados', 'items' => [
            ['label' => 'Listar1', 'url' => ['/site/listar']],   
            ['label' => 'Listar2', 'url' => ['/site/listar2']],
            ['label' => 'Listar3', 'url' => ['/site/listar3']],
            ['label' => 'Listar4', 'url' => ['/site/listar4']],
            ['label' => 'Listar5', 'url' => ['/site/listar5']],
            ['label' => 'Listar6', 'url' => ['/site/listar6']],
                ]],
            ['label' => 'Mostrar', 'items' => [
            ['label' => 'Mostrar', 'url' => ['/site/mostrar']],
            ['label' => 'Mostrar Registro', 'url' => ['/site/mostraruno']],   
            ]]],
        
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Alpe Formacion 2018<?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
